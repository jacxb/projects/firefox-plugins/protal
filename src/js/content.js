console.log("Page loaded");

function get_dark_mode() {
	if (window.matchMedia && !!window.matchMedia('(prefers-color-scheme: dark)').matches)
	{
		return true
	} else {
		return false
	}
}

function set_bookmark_tree(bookmarkItems) {
	bookmark_tree = bookmarkItems;
}

function onRejected(error) {
  console.log(`An error: ${error}`);
}

let bookmark_tree = [];
let gettingTree = browser.bookmarks.getTree();
gettingTree.then(set_bookmark_tree, onRejected);
